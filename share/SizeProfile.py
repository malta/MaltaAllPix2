import ROOT
import os
from glob import glob
import sys
import random
import numpy as np

#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("~/AtlasStyleBase/AtlasUtils.C")
#ROOT.SetAtlasStyle()


ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)
x, y = ROOT.Long(0), ROOT.Long(0)
pitch=36.4
#gStyle.SetPadBorderMode(0)
#gStyle.SetFrameBorderMode(0)


def getProfile(h, name):
    return h.ProfileX(name,-1,100000); #"s" #"bo


f=ROOT.TFile("../output/run_000042/RCEWriter/ana_gbl_000042.root")
#f.ls()
histo2d=f.Get("ClSizeVSAngle")

f2=ROOT.TFile("../output/run_000043/RCEWriter/ana_gbl_000043.root")
histo2db=f2.Get("ClSizeVSAngle")

f3=ROOT.TFile("../output/run_000044/RCEWriter/ana_gbl_000044.root")
histo2dc=f3.Get("ClSizeVSAngle")

can=ROOT.TCanvas("all","all",800,600)
histo2d.SetTitle("all;track incident angle;<cl size>")
histo2d.RebinX(2)  #was 3
histo2db.RebinX(2) #was 3
histo2dc.RebinX(2) #was 3
base=ROOT.TH1D("base","all;track incident angle;<cl size>",10,0,0.55)
#histo2d.GetYaxis().SetRangeUser(1,1.2)
base.SetMinimum(1.0)
base.SetMaximum(1.7)
#histo2d.GetYaxis().SetRangeUser(1,1.2)
#histo2d.Draw("AXIS")
base.Draw("AXIS")
prof=getProfile(histo2d,"vale")
prof.SetLineWidth(2)
prof.SetLineColor(2)
prof.SetMarkerColor(2)
prof.SetMarkerSize(1.2)
prof.SetMarkerStyle(20)
l1=ROOT.TLine(base.GetXaxis().GetXmin(),1.11,base.GetXaxis().GetXmax(),1.11)
l1.SetLineColor(ROOT.kOrange)
l1.SetLineWidth(2)
l1.SetLineStyle(2)
l1.Draw("LSAME")
prof.Draw("SAMEE")

prof2=getProfile(histo2db,"vale2")
prof2.SetLineWidth(2)
prof2.SetLineColor(4)
prof2.SetMarkerColor(4)
prof2.SetMarkerSize(1.2)
prof2.SetMarkerStyle(20)
l2=ROOT.TLine(base.GetXaxis().GetXmin(),1.33,base.GetXaxis().GetXmax(),1.33)
l2.SetLineColor(7)
l2.SetLineWidth(2)
l2.SetLineStyle(2)
l2.Draw("LSAME")
prof2.Draw("SAMEE")

prof3=getProfile(histo2dc,"vale3")
prof3.SetLineWidth(2)
prof3.SetLineColor(8)
prof3.SetMarkerColor(8)
prof3.SetMarkerSize(1.2)
prof3.SetMarkerStyle(20)
l3=ROOT.TLine(base.GetXaxis().GetXmin(),1.49,base.GetXaxis().GetXmax(),1.49)
l3.SetLineColor(3)
l3.SetLineWidth(2)
l3.SetLineStyle(2)
l3.Draw("LSAME")
prof3.Draw("SAMEE")

can.Print("Size.pdf")
sys.exit()

