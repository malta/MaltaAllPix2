#!/usr/bin/env python
########################################################
# Make two correlation plots
# check if there is desync without assumption of alignment
# Based on the code by Valerio.Dao@cern.ch
# Carlos.Solans@cern.ch
# May 2018
#######################################################

import os
import sys
import ROOT

ROOT.gROOT.SetBatch(1)

def getSize(plane):
    #if plane==7: return [ -0.5,   15.5,-0.5, 63.5 ]
    #if plane==0: return [149.5,  240.5,149.5,300.5 ] #123.5,  255.5,99.5, 300.5] 
    return [0.5, 511.5,0.5, 511.5]

def checkCorrelation(args):
    
    folder="../output/run_%06i/RCEWriter/" % (args.run)

    #os.system( "mkdir -p "+folder )

    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(1)
    #SetAtlasStyle();
    ROOT.gStyle.SetPalette(1)
    ROOT.gStyle.SetPadRightMargin(0.1)
    
    fr=ROOT.TFile("../output/run_%06i/RCEWriter/ououtput_MALTAscope-RCE-data.root"%(args.run))
    
    val1=getSize(args.first)
    base=ROOT.TH2D("All","All;pix X; pix Y",int(val1[1]-val1[0]),val1[0],val1[1],int(val1[1]-val1[0]),val1[0],val1[1])
    
    myT0 = fr.Get("Plane0/Hits")
    myT1 = fr.Get("Plane1/Hits")
    myT2 = fr.Get("Plane2/Hits")
    myT3 = fr.Get("Plane3/Hits")

    c1=ROOT.TCanvas("X","X",600,600)

    for i in range(0,myT0.GetEntries()):
        p0=ROOT.TH2D("p0","p0",int(val1[1]-val1[0])/4,val1[0],val1[1],int(val1[1]-val1[0])/4,val1[0],val1[1])
        myT0.Draw("PixY:PixX>>p0","","goff",1,i)
        p0.SetMarkerSize(0.6)
        p0.SetMarkerStyle(20)

        p1=ROOT.TH2D("p1","p1",int(val1[1]-val1[0])/4,val1[0],val1[1],int(val1[1]-val1[0])/4,val1[0],val1[1])
        myT1.Draw("PixY:PixX>>p1","","goff",1,i)
        p1.SetMarkerSize(0.60)
        p1.SetMarkerStyle(21)
        p1.SetMarkerColor(2)

        p2=ROOT.TH2D("p2","p2",int(val1[1]-val1[0])/4,val1[0],val1[1],int(val1[1]-val1[0])/4,val1[0],val1[1])
        myT2.Draw("PixY:PixX>>p2","","goff",1,i)
        p2.SetMarkerSize(0.55)
        p2.SetMarkerStyle(22)
        p2.SetMarkerColor(8)

        p3=ROOT.TH2D("p3","p3",int(val1[1]-val1[0])/4,val1[0],val1[1],int(val1[1]-val1[0])/4,val1[0],val1[1])
        myT3.Draw("PixY:PixX>>p3","","goff",1,i)
        p3.SetMarkerSize(0.50)
        p3.SetMarkerStyle(23)
        p3.SetMarkerColor(4)

        c1.cd()
        base.SetTitle("ALL_%d" % (i))
        base.Draw("AXIS")
        p0.Draw("SAMEP")
        p1.Draw("SAMEP")
        p2.Draw("SAMEP")
        p3.Draw("SAMEP")

        c1.Print( folder+"/E__"+str(i)+".pdf")

    pass

###########################################################################################################
if __name__=="__main__":
    import argparse

    parser=argparse.ArgumentParser()
    ##parser.add_argument('-f' ,'--file',help="Merged file")
    parser.add_argument('-r' ,'--run'     ,help="Merged file"   , type=int, default=666)
    parser.add_argument('-n' ,'--nevents' ,help="First n events", type=int, default=100000)
    parser.add_argument('-i' ,'--initial' ,help="starting point", type=int, default=0)
    parser.add_argument('-pA','--first'   ,help="first plane"   , type=int, default=0)
    parser.add_argument('-pB','--second'  ,help="second plane"  , type=int, default=0)
    args=parser.parse_args()

    checkCorrelation(args)
    print "Have a stupid day"
    
