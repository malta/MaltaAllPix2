#!/usr/bin/env python
# script to run Allpix with given parameters, run proteus+TBAnalysis on RCE files
# patrick.moriishi.freeman@CERN.ch 
# March 2020
#
#
#
import ROOT
from array import array
import math
import argparse
import os
import sys

###############################################
# runAllpix.py
###############################################1

##########################################
# Input Argument Parsing
##########################################
#
#
#
#
# Example of command (for 4 GeV e-, a sensor bias of 12 V, depletion voltage of 100 V, and 300 um thick sensor):
# python runAllpix.py -t 100 -s 30 -e 3GeV -p e- -d 2 -b 6 -n 1000


parser=argparse.ArgumentParser()
parser.add_argument("-p","--particle",help="particle type",type=str,default="e-")
parser.add_argument("-e","--energy",type=str,help = "particle energy",default="3GeV")
parser.add_argument("-b","--bias",help = "SUB voltage of the DUT",type=int,default=-6)
parser.add_argument("-d","--depletionV",help = "depletion voltage of the DUT", type=int,default=-150)
parser.add_argument("-t","--thickness",help = "thickness of telescope planes",type=int,default=300)
parser.add_argument("-s","--sensorThickness",help ="thickness of sensor in telescope planes",type=int,default=300)
parser.add_argument("-n","--nEvent",help="number of events simulated",type=int,default=1000)
parser.add_argument("-r","--run",help ="run number",type=int,required=True)
parser.add_argument("-th","--thresh",help ="threshold of DUT in electrons",type=int,default =400)
parser.add_argument("-ii", "--isIsotope", help="radioactive source flag", action="store_true")
parser.add_argument("-g","--geoFile",type=str,default="MALTAscope-geo0.conf")

args=parser.parse_args()

part = str(args.particle)
energy = args.energy
bias = args.bias
thick = args.thickness
sThick = args.sensorThickness
depV = args.depletionV
nEvents = args.nEvent
run = args.run
thresh = args.thresh
geo = args.geoFile

print " WELCOME TO MALTA ALLPIX2 !!!!! :)"
print " SIMULATION OF MALTA TELESCOPE IN AllPix2 "
print " TRACK RECON WITH Proteus "
print " ANALYSIS WITH TBAanalysis "
print "           ...           "
print "!"


# paths
allPixPath = "/home/sbmuser/MaltaSW/MaltaAllPix2"
configPath = "%s/configs" % allPixPath
outPath = "%s/output/" %allPixPath

#command ="cp %s/main.conf %s/%s_%s.conf" %(configPath, configPath, name, energy)
#os.system(command)

#log run - should do this on a google sheet as well
logFile = open(outPath+"AllPix2Log.txt", "a")
logFile.write("run,%i," %run)
logFile.write("particle,%s," %part)
logFile.write("energy,%s," %energy)
logFile.write("thicnkess,%i," %thick)
logFile.write("sensorThickness,%i," %sThick)
logFile.write("depletionVoltage,%i," %depV)
logFile.write("biasVoltage,%i," %bias)
logFile.write("nEvents,%i," %nEvents)
logFile.write("\n")

logFile.close()

print "Logged run in file"+logFile.name

# make copy of configuration with changes to energy, particles, and name of output directory, etc.

inFile = open("%s/main_g.conf" %(configPath), 'r')
print "%s/main_g.conf" %(configPath)
outFile = open("%s/run_%06i.conf" %(configPath, run), 'w')

for line in inFile:
   newLine = line
   var = line.split("=")[0]
 
   if var =="source_energy ":
     print "This line has the energy"
     newLine = var+" = "+energy+"\n"
     print "old line "+line 
     print "new line "+newLine
 
   if var =="particle_type ":
     print "This line has the particle type"
     newLine = var+" =  \""+part+"\"\n"
     print "old line "+line 
     print "new line "+newLine

   if var =="output_directory ":
     print "This line has the output directory"
     newLine = var+" = \"%s/run_%06i\"\n" % (outPath,run)
     print "old line "+line 
     print "new line "+newLine

   if var =="number_of_events ":
     print "This line has the number of events"
     newLine = var+" = %i\n" % (nEvents)
     print "old line "+line 
     print "new line "+newLine

   if var =="bias_voltage":
     newLine = var+"= %iV\n" % (bias)
     print "old line "+line 
     print "new line "+newLine
 
   if var =="depletion_voltage":
     newLine = var+"= %iV\n" % (depV)
     print "old line "+line 
     print "new line "+newLine

   if var =="source_type " and args.isIsotope:
      newLine = var+"= \"point\"\n"
      print "old line "+line 
      print "new line "+newLine
   
   if var =="threshold" and thresh:
      newLine = var+"= %ie\n" %thresh
      print "old line "+line 
      print "new line "+newLine
   
   if var =="detectors_file ":
      newLine = var+"= /home/sbmuser/MaltaSW/MaltaAllPix2/configs/%s\n" %geo
      print "old line "+line 
      print "new line "+newLine

   outFile.write(newLine)


outFile.close()

# make a copy of the sensor file with chip and active volume thickness

inMalta = open("%s/MALTA_original.conf" %(configPath), 'r')
print "%s/MALTA_original.conf" %(configPath)
outMalta = open("%s/MALTA.conf" %(configPath), 'w')

for line in inMalta:
   newLine = line
   var = line.split("=")[0]
   if var =="chip_thickness ":
     newLine = var+"= %ium \n" % (thick)
     print "old line "+line 
     print "new line "+newLine
 
   if var =="sensor_thickness ":
     newLine = var+"= %ium \n" % (sThick)
     print "old line "+line 
     print "new line "+newLine


   outMalta.write(newLine)


outMalta.close()


# run AllPix2

command2 = "allpix -c %s/run_%06i.conf" % (configPath,run)
print command2
os.system(command2)


###############################################
# Run MaltaDQAllpix
###############################################
#run proteus

#run TBAnalysis
