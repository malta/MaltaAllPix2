#!/usr/bin/env python

import ROOT
import argparse



parser=argparse.ArgumentParser()
parser.add_argument("-f", "--file",type=str, nargs='+')
parser.add_argument("-o", "--output",type=str, default="signal")
args=parser.parse_args()

for f in args.file:
  #open file 
  inFile =  ROOT.TFile(f)
  inFile.cd("Plane0")
  inTree = ROOT.TTree()
  inTree = inFile.Get("Hits")
  #get value histogram
  inTree.Print()
  inTree.Draw("Value")

