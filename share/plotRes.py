import ROOT

ROOT.gStyle.SetOptStat(1)
runs = [60, 61,62, 63, 64, 65, 66, 67, 68, 69, 70]

gCluster = ROOT.TGraphErrors()
gResX = ROOT.TGraphErrors()

count = 0

output = open("../plots/residuals.txt","w+")
output.write("resX \t resX gbl \t resY \t res Y gbl \n ")
for run in runs:
	f = ROOT.TFile("../output/run_0000"+str(run)+"/RCEWriter/Tracked-gbl-0000"+str(run)+"-data.root ","READ")
	fg = ROOT.TFile("../output/run_0000"+str(run)+"/RCEWriter/Tracked-straight-0000"+str(run)+"-data.root ","READ")
	#~ gCluster.SetPointError()
	if run <66:
		treeC = f.Get("Plane3/Clusters")
		treeI = f.Get("Plane3/Intercepts")
		treeCg = fg.Get("Plane3/Clusters")
		treeIg = fg.Get("Plane3/Intercepts")
	else:
	    treeC = f.Get("Plane2/Clusters")
	    treeI = f.Get("Plane2/Intercepts")
	    treeCg = fg.Get("Plane2/Clusters")
	    treeIg = fg.Get("Plane2/Intercepts")
	
	treeC.AddFriend(treeI)
	treeCg.AddFriend(treeIg)
	
	hResX = ROOT.TH1F("res_X", "\\text{X Residuals;track intercept error #Delta x (#mu m);Entries", 300, -300, 300)
	hResY = ROOT.TH1F("res_Y", "Y Residuals;track intercept error #Delta y (#mu m);Entries", 300, -300, 300)
	hgResX = ROOT.TH1F("res_Xgbl", "X Residuals;track intercept error #Delta x (#mu m);Entries", 300, -300, 300)
	hgResY = ROOT.TH1F("res_Ygbl", "X Residuals;track intercept error #Delta y (#mu m);Entries", 300, -300, 300)
	
	n = treeC.GetEntries()

	for i in range(0, n):
		U = treeC.GetLeaf("U")
		U.GetBranch().GetEntry(i)
		U = U.GetValue()
		V = treeC.GetLeaf("V")
		V.GetBranch().GetEntry(i)
		V = V.GetValue()
		Col = treeC.GetLeaf("Col")
		Col.GetBranch().GetEntry(i)
		Col = Col.GetValue()
		Row = treeC.GetLeaf("Row")
		Row.GetBranch().GetEntry(i)
		Row = Row.GetValue()
		resX = float(Col)*36.4-float(U)-256.0*36.4 +18.2
		resY = float(Row)*36.4-float(V)-256.0*36.4 +18.2
		hResX.Fill(resX)
		hResY.Fill(resY)
		
		U = treeCg.GetLeaf("U")
		U.GetBranch().GetEntry(i)
		U = U.GetValue()
		V = treeCg.GetLeaf("V")
		V.GetBranch().GetEntry(i)
		V = V.GetValue()
		Col = treeCg.GetLeaf("Col")
		Col.GetBranch().GetEntry(i)
		Col = Col.GetValue()
		Row = treeCg.GetLeaf("Row")
		Row.GetBranch().GetEntry(i)
		Row = Row.GetValue()
		resX = float(Col)*36.4-float(U)-256.0*36.4 +18.2
		resY = float(Row)*36.4-float(V)-256.0*36.4 +18.2
		hgResX.Fill(resX)
		hgResY.Fill(resY)
		
	fX = ROOT.TF1("fX","gaus",-300,300)
	fX.SetLineColor(1)
	fY = ROOT.TF1("fY","gaus",-300,300)
	fY.SetLineColor(1)
	fgX = ROOT.TF1("fgX","gaus",-300,300)
	fgX.SetLineColor(2)
	fgY = ROOT.TF1("fgY","gaus",-300,300)
	fgY.SetLineColor(2)
	
	canX = ROOT.TCanvas()
	#hResX.Rebin(5000)
	hResX.GetXaxis().SetRangeUser(-300, 300)
	hResX.Fit(fX,"r+")
	hResX.Draw("same")	
	hgResX.SetLineColor(2)
	hgResX.Draw("same")	
	hgResX.Fit(fgX,"r+")
	canX.Print("../plots/run_0000"+str(run)+"ResX.pdf")
	
	canY = ROOT.TCanvas()
	#hResX.Rebin(5000)
	hResY.GetXaxis().SetRangeUser(-300, 300)
	hResY.Fit(fY,"r+")
	hResY.Draw("same")	
	hgResY.SetLineColor(2)
	hResY.Fit(fgY,"r+")
	hgResY.Draw("same")	
	canY.Print("../plots/run_0000"+str(run)+"ResY.pdf")
	
	output.write("%.2f \t %.2f \t %.2f \t %.2f  \n" %(fX.GetParameter(2), fX.GetParameter(2),  fgX.GetParameter(2),  fgY.GetParameter(2))  )
	#gResX.SetPoint( count, biases[count],hCluster.GetMean())
	count+=1

output.close()



raw_input("Press any key")
print ("Have a decent evening")


