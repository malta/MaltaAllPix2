#!/usr/bin/env python
#copied from PS08 MaltaDQ_DESY_oct_2019.py
# want to use names, not run numbers
# can skip merging since RCE files are already merged
# patrick.moriishi.freeman@CERN.ch 
# March 2020
import os
import os.path
import sys
#import pexpect
import getpass
import argparse
import glob
import base64
import ROOT
#import autoDQ

###start of Patrick's edits to convert from DESY to Allpix2 analysis
#change the base path
path     ="/home/sbmuser/MaltaSW/MaltaAllPix2/"#"../"

#skip web stuff for now
'''
webfolder="/home/sbmuser/DESY_oct2019/web/"
os.system("mkdir -p "+webfolder)
adecmospassword = base64.b64decode("S2lnZUJlbGUxOQ==")#getpass.getpass("Enter adecmos password:")
'''
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--run"     , help="Run number"              , type=int           , required=True)

parser.add_argument("-a", "--analysis", help="analysis only Run number", action="store_true")
args=parser.parse_args()

run      =args.run
aONLY    =args.analysis
#edit path of output data
pathRun="%s/output/run_%06i/RCEWriter/"%(path,run)
os.system("mkdir -p %s/output/run_%06i/RCEWriter"%(path,run))

##skip this stuff about merging, i think we are ok. Only missing L1ID and BCID from RCE files output by allpix

######################################################################
local_merged ="%s/ououtput_MALTAscope-RCE-data.root" %pathRun

pathP ="/home/sbmuser/MaltaSW//MaltaTbAnalysis" 
baseFolder=pathP+"/DESY_oct2019/config/"#"/cosmics_tests/config/"


#alignFile="%s/output_MALTAscope-RCE-geo.toml" %pathRun
alignFile= "/DESY_oct2019/config/geometry_3GeV.toml"#geometry_Sr90_short.toml"


#if run>=7140 and run<=7168:
#    alignFile="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/run_007150/FULL-007150-geo.toml"


commandBase ="pt-align -d "+baseFolder+"device.toml "

commandBase+=" -c "+baseFolder+"analysis_AllPix.toml "
'''

print " "

print " "
myCommand=commandBase+" -u tel_fine -n 1000000 "
#myCommand+="-g "+pathRun+("TELcoarse-%06i-geo.toml" % (run))
###myCommand+=" -g "+baseFolder+"geometry_cosmics_betterAlign.toml "
myCommand+=" -g /home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/config/geometry_3GeV.toml"
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u tel_fine3 -n 1000000 "
myCommand+="-g "+pathRun+("TELfine-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine3-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



print " "
myCommand=commandBase+" -u dut_fine -n 1000000 "
#myCommand+="-g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))
myCommand+="-g "+pathRun+("TELfine3-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("FULL-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



##################################################################################

print "############ RUN TRACKING #############"

local_track_Straight   = pathRun+"Tracked-%06i" % (run)
command="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis_AllPix.toml "
#command+=" -g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_Straight
#command+=" -g "+baseFolder+"geometry_cosmics_betterAlign.toml"+" -u straight "+local_merged+" "+local_track_Straight
command+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_Straight
defaultAlign="/home/sbmuser/MaltaSW/MaltaTbAnalysis/DESY_oct2019/config/geometry_3GeV.toml"
#command+=" -g "+defaultAlign+" -u straight "+local_merged+" "+local_track_Straight
print command
if not aONLY: os.system(command)


local_track_gblTracking   = pathRun+"Tracked-gbl-%06i" % (run)
command2="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis_AllPix.toml "
#command2+=" -g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))+" -u gbl "+local_merged+" "+local_track_gblTracking
command2+=" -g "+baseFolder+"geometry_3GeV.toml"+" -u gbl "+local_merged+" "+local_track_gblTracking

print command2
if not aONLY: os.system(command2)


'''
'''
print " "
myCommand=commandBase+" -u tel_coarse -n 1500000 "
myCommand+="-g "+alignFile
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELcoarse-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)

print " "
myCommand=commandBase+" -u tel_fine -n 1500000 "
myCommand+="-g "+pathRun+("TELcoarse-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)

print " "
myCommand=commandBase+" -u tel_fine3 -n 1500000 "
myCommand+="-g "+pathRun+("TELfine-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine3-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u dut_coarse -n 1500000 "
myCommand+="-g "+pathRun+("TELfine3-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("DUTcoarse-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u dut_fine -n 1500000 "
myCommand+="-g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("FULL-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



##################################################################################

print "############ RUN TRACKING #############"
local_track_straightTrack = pathRun+"Tracked-straight-%06i" % (run)
local_track_gblTracking   = pathRun+"Tracked-gbl-%06i" % (run)
command="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
if run>=6689 and run<=6712:
  command="pt-track -d "+baseFolder+"device_MALTAC.toml -c "+baseFolder+"analysis.toml "
#command+=" -g "+pathRun+("DUTfine1-%06i-geo.toml" % (run))+" "+local_merged+" "+local_track
#command+=" -g "+pathRun+("TELfine-%06i-geo.toml" % (run))+" "+local_merged+" "+local_track
command+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_straightTrack
#myCommand+=" -m "+pathRun+("TELNoise-%06i" % (run))
#myCommand+=" -m "+pathRun+("DUTNoise-%06i" % (run))
command2="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
if run>=6689 and run<=6712:
  command2="pt-track -d "+baseFolder+"device_MALTAC.toml -c "+baseFolder+"analysis.toml "
command2+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u gbl "+local_merged+" "+local_track_gblTracking
#print command
#if not aONLY: os.system(command)

print command2
if not aONLY: os.system(command2)
'''

print " "
myCommand=commandBase+" -u tel_coarse -n 1500000 "
myCommand+="-g "+pathP+alignFile
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELcoarse-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)

print " "
myCommand=commandBase+" -u tel_fine -n 1500000 "
myCommand+="-g "+pathRun+("TELcoarse-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)

print " "
myCommand=commandBase+" -u tel_fine3 -n 1500000 "
myCommand+="-g "+pathRun+("TELfine-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("TELfine3-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u dut_coarse -n 1500000 "
myCommand+="-g "+pathRun+("TELfine3-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("DUTcoarse-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)


print " "
myCommand=commandBase+" -u dut_fine -n 1500000 "
myCommand+="-g "+pathRun+("DUTcoarse-%06i-geo.toml" % (run))
myCommand+=" "+local_merged+" "
myCommand+=pathRun+("FULL-%06i" % (run))
print myCommand
if not aONLY: os.system(myCommand)



##################################################################################

print "############ RUN TRACKING #############"
local_track_straightTrack = pathRun+"Tracked-straight-%06i" % (run)
local_track_gblTracking   = pathRun+"Tracked-gbl-%06i" % (run)
command="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
if run>=6689 and run<=6712:
  command="pt-track -d "+baseFolder+"device_MALTAC.toml -c "+baseFolder+"analysis.toml "
#command+=" -g "+pathRun+("DUTfine1-%06i-geo.toml" % (run))+" "+local_merged+" "+local_track
#command+=" -g "+pathRun+("TELfine-%06i-geo.toml" % (run))+" "+local_merged+" "+local_track
command+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u straight "+local_merged+" "+local_track_straightTrack
#myCommand+=" -m "+pathRun+("TELNoise-%06i" % (run))
#myCommand+=" -m "+pathRun+("DUTNoise-%06i" % (run))
command2="pt-track -d "+baseFolder+"device.toml -c "+baseFolder+"analysis.toml "
if run>=6689 and run<=6712:
  command2="pt-track -d "+baseFolder+"device_MALTAC.toml -c "+baseFolder+"analysis.toml "
command2+=" -g "+pathRun+("FULL-%06i-geo.toml" % (run))+" -u gbl "+local_merged+" "+local_track_gblTracking
#print command
#if not aONLY: os.system(command)

print command2
if not aONLY: os.system(command2)

#should work up to here, barring some minutiae. The missing BCID and L1ID should be a problem

######################################################################
#local_tracked_straight=local_track_straightTrack+"-data.root"
local_tracked_gbl=local_track_gblTracking+"-data.root"
print "---Running TBAnalysis"
local_tbana_straight="%s/ana%06i.root" % (pathRun,run)
local_tbana_gbl     ="%s/ana_gbl_%06i.root" % (pathRun,run)
#if os.path.exists(local_tracked_straight):
#    cmd="TBAnalysis %s %s %i" % (local_tracked_straight,local_tbana_straight,run)
#    print cmd
#    os.system(cmd)
#else:
#    sys.exit()

if os.path.exists(local_tracked_gbl):
    cmd="TBAnalysis %s %s %i" % (local_tracked_gbl,local_tbana_gbl,run)
    print cmd
    os.system(cmd)
else:
    pass
    ###sys.exit()

'''

print "------------------------------------------------------------------------------------------------------"
print "---Running autoDQ"
local_dqana_straight="%s/DQ_%06i.txt" % (pathRun,run)
##autoDQ.autoDQ(local_tbana_straight,local_dqana_straight,0.10)

local_dqana_gbl="%s/DQ_gbl_%06i.txt" % (pathRun,run)
autoDQ.autoDQ(local_tbana_gbl,local_dqana_gbl,0.10)
#local_dqana_2=""
#if local_tbana_2!="":
#    local_dqana_2="%s/DQ_%06i_c2.txt" % (local_proc,args.run)
#    autoDQ.autoDQ(local_tbana_2,local_dqana_2,0.10)

######################################################################

local_web = "%s/run_%06i" % (webfolder,run)
os.system("mkdir -p %s" % local_web)
os.system("cp %s %s/." % (local_dqana_straight,local_web))
os.system("cp %s %s/." % (local_tbana_gbl,local_web))
os.system("chmod -R 777 "+local_web)

#fuck it this is too slow

eosPath="/eos/user/a/adecmos/www/ade-pixel-testbeam/files/"
query = "scp -r %s adecmos@lxplus.cern.ch:%s/%s" % (local_web, eosPath, "ELSA_Apr2019")
print query
try:
    child = pexpect.spawn(query)
    child.expect(["Password: "])
    child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=5)
    child.interact()
except:
    print "MOTHERFUCKER I wasn't able to copy the file .... "
    os.system(query)
'''
