#~ telescope
#~ python runAllpix.py -r 42 -e 2GeV -p Mu- -n 30000 -t 100  -d -400 -b -6 -s 30 -th 550
#~ python runAllpix.py -r 43 -e 2GeV -p Mu- -n 30000 -t 100  -d -400 -b -6 -s 30 -th 360
#~ python runAllpix.py -r 44 -e 2GeV -p Mu- -n 30000 -t 100  -d -400 -b -6 -s 30 -th 280

#~ DESY
#~ python runAllpix.py -r 45 -e 3GeV -p e- -n 30000 -t 100  -d -200 -b -6 -s 30 -th 410
#~ python runAllpix.py -r 46 -e 3GeV -p e- -n 30000 -t 100  -d -200 -b -12 -s 30 -th 410
#~ python runAllpix.py -r 47 -e 3GeV -p e- -n 30000 -t 100  -d -200 -b -20 -s 30 -th 410
#~ python runAllpix.py -r 48 -e 3GeV -p e- -n 30000 -t 100  -d -200 -b -25 -s 30 -th 410
#~ python runAllpix.py -r 49 -e 3GeV -p e- -n 30000 -t 100  -d -200 -b -30 -s 30 -th 410

#~ python MaltaDQ_Allpix.py -r 45
#~ python MaltaDQ_Allpix.py -r 46
#~ python MaltaDQ_Allpix.py -r 47  
#~ python MaltaDQ_Allpix.py -r 48
#~ python MaltaDQ_Allpix.py -r 49

#~ python SizeProfile.py
#~ python plotCluster.py

python runAllpix.py -r 60 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_12cm_25cm.conf
python runAllpix.py -r 61 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_5cm_5cm.conf
python runAllpix.py -r 62 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_5cm_2cm.conf
python runAllpix.py -r 63 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_2cm_20cm.conf
python runAllpix.py -r 64 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_2cm_5cm.conf
python runAllpix.py -r 65 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_6p_2cm_2cm.conf

python runAllpix.py -r 66 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_4p_5cm_5cm.conf
python runAllpix.py -r 67 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_4p_5cm_2cm.conf
python runAllpix.py -r 68 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_4p_2cm_20cm.conf
python runAllpix.py -r 69 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_4p_2cm_5cm.conf
python runAllpix.py -r 70 -e 3MeV -p e- -n 10000 -t 300 -th 300  -s 30  -g MALTAscope_4p_2cm_2cm.conf

python MaltaDQ_Allpix_g.py -r 60 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000060/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 61 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000061/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 62 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000062/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 63 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000063/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 64 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000064/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 65 -d device_6p.toml -A analysis_6p.toml -g ../output/run_000065/RCEWriter/output_MALTAscope-RCE-geo.toml 

python MaltaDQ_Allpix_g.py -r 66 -d device_4p.toml -A analysis_4p.toml -g ../output/run_000066/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 67 -d device_4p.toml -A analysis_4p.toml -g ../output/run_000067/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 68 -d device_4p.toml -A analysis_4p.toml -g ../output/run_000068/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 69 -d device_4p.toml -A analysis_4p.toml -g ../output/run_000069/RCEWriter/output_MALTAscope-RCE-geo.toml 
python MaltaDQ_Allpix_g.py -r 70 -d device_4p.toml -A analysis_4p.toml -g ../output/run_000070/RCEWriter/output_MALTAscope-RCE-geo.toml 

python  plotRes.py 
