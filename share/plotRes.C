//
//  plot_residuals.cpp
//  
//
//  Created by Patrick M Freeman on 2/5/19.
//
#include <iostream>
#include <fstream>
using namespace std;

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TStyle.h>

int plotResiduals(const char *fileName = "./Tracked-data-electrons.root",
                  const string plotNameBase = "resolution",
                  const char *fileNameRes = "resolution.txt",
                  const bool debug = false, const int nevents = 50000,
                  const int NBINS = 100,
                  const int MIN = -100,
                  const int MAX = 100){
    
    TH1F *h1ResX = new TH1F("h1ResX", "Residuals x", NBINS, MIN, MAX);
    TH1F *h1ResY = new TH1F("h1ResY", "Residuals y", NBINS, MIN, MAX);
    
    cout <<  ": opening file " << fileName << endl;
    TFile *file = TFile::Open(fileName);
    if(file == NULL){
        cout <<  ": ERROR!!! - cannot open file " << fileName << endl;
        return 1;
    }
    
    TTree *tree = (TTree *) file -> Get("Plane4/Clusters");
    if(tree == NULL){
        cout <<  ": ERROR!!! - cannot load intercept tree" << endl;
        return 1;
    }
    TTree *treeI = (TTree *) file -> Get("Plane4/Intercepts");
    tree->AddFriend(treeI);
    
    const unsigned int nEntries = tree -> GetEntries();
    
    double U, V, Col, Row;
    int Track;
    TBranch *b_Row, *b_Col, *b_U, *b_V, *b_Track;
    tree -> SetBranchAddress("Row", &Row, &b_Row);
    tree -> SetBranchAddress("Col", &Col, &b_Col);
    treeI -> SetBranchAddress("U", &U, &b_U);
    treeI -> SetBranchAddress("V", &V, &b_V);
    treeI -> SetBranchAddress("Track", &Track, &b_Track);



    cout <<  ": nEntries = " << nEntries << endl;
    cout<< nEntries <<endl;
    for( int iEntry=0; iEntry<nevents; iEntry++){
        Track =1;
        tree -> GetEntry(iEntry);
        //treeI -> GetEntry(iEntry);
        const double resX = Col*36.4-U-256*36.4;
        const double resY = Row*36.4-V-256*36.4;
        if(debug) cout<< iEntry << "    " << nEntries <<endl;
        if(Track==0){
            if(debug) cout<< "U =" << U <<endl;
            if(debug) cout<< "Col =" << Col <<endl;
            if(debug) cout<< "resX =" << resX <<endl;
            if(debug) cout << "cov[x,y] = ( " << resX << " , " << resY << " ) " << endl;
            h1ResX -> Fill(resX);
            h1ResY -> Fill(resY);
        }
        if(iEntry>=nevents){break;}
    }
    
    TF1 *fitx = new TF1("FResX","gaus", MIN, MAX);
    TF1 *fity = new TF1("FResY","gaus", MIN, MAX);

    cout <<  ": closing file " << fileName << endl;
    delete tree;
    file -> Close();
    delete file;
    
    TCanvas *cc = new TCanvas("cc", "cc", 0, 0, 1000, 500);
    gStyle->SetOptFit();
    cc -> Divide(2,1);
    cc -> cd(1);
    h1ResX -> GetXaxis() -> SetTitle("track intercept error #Deltax #left[#mum#right]");
    h1ResX -> GetYaxis() -> SetTitle("Events");
    h1ResX -> Draw();
    h1ResX->Fit("FResX");

    cc -> cd(2);
    h1ResY -> GetXaxis() -> SetTitle("track intercept error #Deltay #left[#mum#right]");
    h1ResY -> GetYaxis() -> SetTitle("Events");
    h1ResY -> Draw();
    h1ResY->Fit("FResY");

    cc -> SaveAs((plotNameBase + ".png").c_str());
    cc -> SaveAs((plotNameBase + ".root").c_str());
    cc -> SaveAs((plotNameBase + ".pdf").c_str());
    cc -> SaveAs((plotNameBase + ".C").c_str());
    
    cout <<  ": writing resolutions to file " << fileNameRes << endl;
    ofstream fileRes(fileNameRes);
    if(!fileRes){
        cout <<  ": ERROR!!! - cannot open file " << fileNameRes << endl;
        return 1;
    }
    
    fileRes << "res x = " << h1ResX -> GetMean() << " +/- " << h1ResX -> GetRMS()<< endl;
    fileRes << "res y = " << h1ResY -> GetMean() << " +/- " << h1ResY -> GetRMS()<< endl;
    
    fileRes << " From Gaussian fits:"<<endl;
    fileRes << "res x = " << fitx -> GetParameter(1) << " +/- " << fitx -> GetParameter(2)<< endl;
    fileRes << "res y = " << fity -> GetParameter(1) << " +/- " << fity -> GetParameter(2)<< endl;

    fileRes.close();

    return 0;
}
