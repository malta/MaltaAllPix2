import ROOT

ROOT.gStyle.SetOptStat(1)
biases = [6, 12, 20, 25, 30]
runs = [51]

gCluster = ROOT.TGraphErrors()
gResX = ROOT.TGraphErrors()

count = 0

for run in runs:
	f = ROOT.TFile("../output/run_0000"+str(run)+"/RCEWriter/ana_gbl_0000"+str(run)+".root ")
	hCluster = f.Get("ClSize_match")
	gCluster.SetPoint( count, biases[count],hCluster.GetMean())
	#~ gCluster.SetPointError()
	hResX = f.Get("res_X")
	#gResX.SetPoint( count, biases[count],hCluster.GetMean())
	count+=1

can = ROOT.TCanvas()
gCluster.Draw()	
can.Print("../plots/ClSUB.pdf")

can2 = ROOT.TCanvas()
hResX.Rebin(5000)
hResX.GetXaxis().SetRangeUser(-200, 200)
hResX.Draw("")	
can.Print("../plots/ResX.pdf")

raw_input("Press any key")
print ("Have a decent evening")


